"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import os
import sys
import argparse
import warnings

from lib.frank_energy import optimize_frank_energy
from lib.varia import experiment_name, stream_tee, safe_mkdir

# Using GPU may result in a slower execution on some hardware ...
# os.environ["CUDA_VISIBLE_DEVICES"] = '-1'

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
warnings.simplefilter("ignore")

parser = argparse.ArgumentParser()
parser.add_argument('--eta', type=float,
                    default=0.01,
                    help='Ratio between the radii of the torus')
parser.add_argument('--k3', type=float,
                    default=1,
                    help='Ratio of the bend to twist elastic constants')
parser.add_argument('--k24', type=float,
                    default=2,
                    help='Ratio of the saddle-splay to twist elastic constants')
parser.add_argument('--steps', type=int,
                    default=16000,
                    help='Minimal number of iterations performed')
parser.add_argument('--plot', type=bool,
                    default=True,
                    help='Plot experiment results')
parser.add_argument('--save', type=bool,
                    default=False,
                    help='Save experiment results and plots')
FLAGS, _ = parser.parse_known_args()

# parameters
eta_val = FLAGS.eta
k3_val = FLAGS.k3
k24_val = FLAGS.k24

run_name = experiment_name(eta_val, k3_val, k24_val)

safe_mkdir('logs')
logfile = open(os.path.join('logs', '{0}.txt'.format(run_name)), 'w+')
sys.stdout = stream_tee(sys.stdout, logfile)

if FLAGS.save:
    safe_mkdir('results')

# perform optimization
optimize_frank_energy(eta_val=eta_val, k3_val=k3_val, k24_val=k24_val, run_name=run_name, steps=FLAGS.steps,
                      save_results=FLAGS.save, plot_show=FLAGS.plot,
                      results_home='results')

logfile.close()
