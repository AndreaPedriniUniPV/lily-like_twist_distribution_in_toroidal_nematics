"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import numpy as np
import tensorflow as tf

import lib.config as c


###########################################
# real functions (to be defined in [0,1]) #
###########################################

def parallels(s):
    """parallels is the null function.

        Args:
            s: tensor (sigma-sampling of the real interval [0,1]).

        Returns:
            tensor with the same shape as s and value:
                0 * s

    """
    return 0. * s


def lin_function(s, eta):
    """lin_function is the linear test function.

        Args:
            s: np.array (sigma-sampling of the real interval [0,1]);
            eta: scalar (geometric parameter).

        Returns:
            np.array with the same shape as s and value:
                eta * s

    """
    return np.multiply(eta, s)


def beta(eta, k24, a_1=c.a_1):
    """beta is the coefficient in the rational qua function.

    Args:
        eta: scalar (geometric parameter);
        k24: scalar (elastic constant);
        a_1: boolean value of the constrain a(1)=0.

    Returns:
        scalar: 1 if a_1, 2 * (1 - k24) / (3 - eta ** 2 - 2 * k24) otherwise

    """
    if a_1:
        b = 1
    else:
        b = 2 * (1 - k24) / (3 - eta ** 2 - 2 * k24)
    return b


def qua_function(s, eta, k24, a_1=c.a_1):
    """qua_function is the rational test function.

    Args:
        s: np.array (sigma-sampling of the real interval [0,1]);
        eta: scalar (geometric parameter);
        k24: scalar (elastic constant);
        a_1: boolean value of the constrain a(1)=0.

    Returns:
        An array with the same shape as s and value:
            eta * s * (1 - beta(eta, k24, a_1) * s)

    """
    return np.multiply(eta, s * (1 - beta(eta, k24, a_1) * s))


def gamma(s, k3, k24):
    """gamma is the estimated angle alpha in the cylindrical case, for eta=0 and k24>1.

        Args:
            s: array (sigma-sampling of the real interval [0,1]);
            k3: scalar (elastic constant);
            k24: scalar (elastic constant).

        Returns:
            np.array with the same shape as s and value:
                arctan(2 * sqrt(k24 * (k24 - 1)) * s / (sqrt(k3) * (k24 - (k24 - 1) * s**2)))

    """
    return np.arctan(2 * np.sqrt(k24 * (k24 - 1)) * s / (np.sqrt(k3) * (k24 - (k24 - 1) * (s ** 2))))


def cylinder(s, k3, k24):
    """cylinder is the estimated a in the cylindric case, for eta=0 and k24>1.

        Args:
            s: np.array (sigma-sampling of the real interval [0,1]);
            k3: scalar (elastic constant);
            k24: scalar (elastic constant).

        Returns:
            np.array with the same shape as s and value:
                sin(gamma(s,k3,k24))

    """
    return np.sin(gamma(s, k3, k24))


def approximator_tanh(s, init, a_0=c.a_0, a_1=c.a_1, size=c.size, depth=c.depth, scope='a_tanh'):
    """approximator builds the approximator with tanh as activation function and depth fully connected layers.
        a_0 and a_1 conditions are both analytical

        Args:
            s: tensor (sigma-sampling of the real interval [0,1]);
            init: initializer for the variables;
            size: scalar (dimension of hidden layers)
            a_0: boolean value of the constrain a(0)=0;
            a_1: boolean value of the constrain a(1)=0;
            depth: scalar (number of hidden layers);
            scope: string (scope of the variables).

        Returns:
            A tensor with the same shape as s.

    """
    with tf.variable_scope(scope):

        # make s a 2-dim tensor
        s_expanded = tf.expand_dims(s, axis=1)

        # layer 0
        hidden_scope = '0'
        with tf.variable_scope(hidden_scope):
            w = tf.get_variable('w', shape=(size, 1), initializer=init, dtype=tf.float32)
            b = tf.get_variable('b', shape=(size, 1), initializer=init, dtype=tf.float32)
            mm = tf.matmul(w, tf.transpose(s_expanded), name='mm')
            h = tf.tanh(mm + b, name='h')
            # at_0 and at_1 are necessary to compute b and w_last (see below)
            at_0 = tf.tanh(b, name='at_0')
            at_1 = tf.tanh(w + b, name='at_1')

        # layers from 0 to depth-1
        for i in range(1, depth - 1):
            hidden_scope = '{0}'.format(i)
            with tf.variable_scope(hidden_scope):
                w = tf.get_variable('w', shape=(size, size), initializer=init, dtype=tf.float32)
                b = tf.get_variable('b', shape=(size, 1), initializer=init, dtype=tf.float32)
                mm = tf.matmul(w, h, name='mm')
                h = tf.tanh(mm + b, name='h')
                at_0 = tf.tanh(tf.matmul(w, at_0) + b, name='at_0')
                at_1 = tf.tanh(tf.matmul(w, at_1) + b, name='at_1')

        # last fully connected linear layer (no activation function)
        with tf.variable_scope('{0}'.format(depth)):

            # the values assigned to b and w_last, respectively, are intended to enforce a(0) = 0 and a(1) = 0
            if a_0 and a_1:
                diff_01 = tf.subtract(at_0, at_1, name='diff_01')
                diff_last = diff_01[-1, :]
                diff_short = diff_01[:-1, :]
                w_short = tf.get_variable('w_short', shape=(size - 1, 1), initializer=init, dtype=tf.float32)
                w_last = tf.subtract(0., tf.divide(tf.matmul(tf.transpose(w_short), diff_short), diff_last),
                                     name='w_last')
                w = tf.concat([w_short, w_last], 0, name='w')
                mm = tf.matmul(tf.transpose(w), h, name='mm')
                b = tf.subtract(0., tf.matmul(tf.transpose(w), at_0), name='b')
            elif a_0:
                w = tf.get_variable('w', shape=(size, 1), initializer=init, dtype=tf.float32)
                mm = tf.matmul(tf.transpose(w), h, name='mm')
                b = tf.subtract(0., tf.matmul(tf.transpose(w), at_0), name='b')
            elif a_1:
                w = tf.get_variable('w', shape=(size, 1), initializer=init, dtype=tf.float32)
                mm = tf.matmul(tf.transpose(w), h, name='mm')
                b = tf.subtract(0., tf.matmul(tf.transpose(w), at_1), name='b')

            else:
                w = tf.get_variable('w', shape=(size, 1), initializer=init, dtype=tf.float32)
                mm = tf.matmul(tf.transpose(w), h, name='mm')
                b = tf.get_variable('b', shape=(1, 1), initializer=init, dtype=tf.float32)
            out = tf.add(mm, b, name='out')
            # make the output a 1-dim tensor
            a = tf.squeeze(out, axis=0, name='a')

    return a


###############
# derivatives #
###############

def derivative(a, s):
    """derivative computes the derivative of a with respect to s;
        transforms None gradients into 0-array;
        squeezes the result.

        Args:
            s: tensor (sigma-sampling of the real interval [0,1]);
            a: tensor.

        Returns:
            tensor with the same shape of a.

    """
    da = tf.gradients(a, s)
    if da == [None]:
        da = tf.scalar_mul(0., s)
    else:
        da = tf.squeeze(da, axis=0)
    return da


##########
# angles #
##########

def alpha(a, s, psi, eta):
    """alpha computes the angle alpha, depending on s and psi.

        Args:
            a: np.array with the same shape as s (a represents a function of s);
            s: np.array (sigma-sampling of the real interval [0,1]);
            psi: np.array (sampling of the real interval [0,2*Pi]);
            eta: scalar (elastic parameter).

        Returns:
            np.array with shape (dim(psi),dim(s)).

    """
    # expand dimensions
    s_expanded = np.expand_dims(s, axis=1)
    psi_expanded = np.expand_dims(psi, axis=1)
    a_expanded = np.expand_dims(a, axis=1)
    # compute the denominator
    den = 1. + eta * np.transpose(s_expanded) * np.cos(psi_expanded)
    # compute the angle
    angle = np.arcsin(np.transpose(a_expanded) / den)
    return angle


def alpha_single(s, psi, a, eta):
    """alpha_single computes the angle alpha, for a specific choice of s and psi.

        Args:
            a: scalar;
            s: scalar (value in [0,1]);
            psi: scalar (value in [0,2*Pi]);
            eta: scalar (geometric parameter).

        Returns:
            scalar.

    """
    # compute the denominator
    den = 1 + eta * s * np.cos(psi)
    # compute the angle
    angle = np.arcsin(a / den)
    return angle


################
# Frank energy #
################

def core_energy(s, a, da, eta, k3, k24, s_index=c.s_index, scope='core_energy'):
    """core_energy compute Frank's Energy.

        Args:
            s: 1D-tensor (sigma-sampling of the real interval [0,1]);
            a: 1D-tensor with the same length as s (a represents a function of s);
            da: 1D-tensor with the same length as s (da represents the derivative of a with respect to s);
            eta: scalar (geometric parameter);
            k3: scalar (elastic constant);
            k24: scalar (elastic constant);
            s_index: the index of s corresponding to value 1;
            scope: string (scope of the variables).

        Returns:
            scalar (Frank's Energy of a minus the energy of the parallels).

    """
    with tf.variable_scope(scope):
        # the Frank energy has a singularity at s=0
        # in order to avoid division by 0, the first values corresponding to s=0 are ignored
        s_short = tf.slice(s, [1], [-1])
        a_short = tf.slice(a, [1], [-1])
        da_short = tf.slice(da, [1], [-1])

        # recurrent factors
        # etas = eta * s
        etas = tf.multiply(eta, s_short, name='etas')
        # sqa = (1 - a) ** 2
        sqa = tf.square(1 - a_short, name='sqa')
        # sqma = (1 - a) ** 2
        sqma = tf.square(1 + a_short, name='sqma')
        # ca = C(a) = sqrt(sqa - etas^2)
        ca = tf.sqrt(sqa - etas ** 2, name='ca')
        # cma = C(-a) = sqrt(sqma - eta^2 * s^2)
        cma = tf.sqrt(sqma - etas ** 2, name='cma')
        # recca = 1 / ca
        recca = tf.divide(1., ca, name='recca')
        # reccma = 1 / cma
        reccma = tf.divide(1., cma, name='reccma')
        # denfact = sqrt((1 - etas^2)
        denfact = tf.sqrt(1. - etas ** 2, name='denfact')

        # integrand components [all of them have positive sign]
        # f1 = (recca + reccma) * s / 2 * da^2
        f1 = tf.multiply((recca + reccma) * s_short / 2, da_short ** 2, name='f1')
        # f2 = (recca - reccma) * da
        f2 = tf.multiply(recca - reccma, da_short, name='f2')
        # f3 = (recca + reccma) * da_ * a
        f3 = tf.multiply((recca + reccma), tf.multiply(da_short, a_short), name='f3')
        # f4 = sqa / (2 * s) * recCa
        f4 = tf.multiply(sqa / (2 * s_short), recca, name='f4')
        # f5 = sqma / (2 * s) * reccma
        f5 = tf.multiply(sqma / (2 * s_short), reccma, name='f5')
        # f6 = 1 / (s * denfact)
        f6 = tf.divide(1., (s_short * denfact), name='f6')
        # f7 =  2 a * da / denfact^3
        f7 = tf.multiply(2., a_short * da_short / denfact ** 3, name='f7')
        # f8 = (2 + 7 * etas^2) / (2 * s * denfact^5) * a^2
        f8 = tf.multiply((2. + 7. * etas ** 2) / (2 * s_short * (denfact ** 5)), a_short ** 2, name='f8')
        # f9 =  (3 * etas^4 + 24 * etas^2 + 8) / (8 * s * denfact^9) * a^4
        f9 = tf.multiply(
            (3. * etas ** 4 + 24. * etas ** 2 + 8.) / (8 * s_short * denfact ** 9),
            a_short ** 4, name='f9')
        # f10 = (3 * eta^2 * s) / denfact^5 * a^2
        f10 = tf.multiply((3. * eta ** 2 * s_short) / denfact ** 5, a_short ** 2, name='f10')
        # f11 = (ca + cma) / (2 * s)
        f11 = tf.divide(ca + cma, 2 * s_short, name='f11')

        # twist = f1 + f2 - f3 + f4 + f5 - f6 + f7 + f8 - f9
        twist = tf.subtract(f1 + f2 + f4 + f5 + f7 + f8, f3 + f6 + f9, name='twist')
        # bend = f6 + f9 - f10 - f11
        bend = tf.subtract(f6 + f9, f10 + f11, name='bend')
        # saddle_splay = - 2 * a_short(1)^2 / den_factor(1)^3
        saddle_splay = tf.multiply(-1., 2. * (a_short[s_index] ** 2) / (denfact[s_index] ** 3),
                                   name='saddle_splay_vector')

        # integrand = twist + k3 * bend
        integrand = tf.add(twist, k3 * bend, name='integrand')

        # integrand of the parallels (0-function)
        # integrand_0 = k3 * (f6 - (1 - etas^2)^(1/2)) / s) = k3 * eta^2 * s / denfact
        integrand_0 = tf.divide(k3 * eta ** 2 * s_short, denfact, name='integrand_0')

        # width of subintervals
        # delta_s[i] = s[i+1] - s[i]
        delta_s = tf.subtract(tf.concat([tf.slice(s_short, [1], [-1]), [s_short[-1]]], 0), s_short,
                              name='delta_s')

        # approximate the integrals
        # Euler-Maclaurin: integral = 1/2 * sum(delta_s[i] * (integrand[i+1] + integrand[i]))
        integral = tf.divide(
            tf.reduce_sum(delta_s * (tf.add(tf.concat([tf.slice(integrand, [1], [-1]), [1]], 0), integrand))), 2.,
            name='integral')
        # energy of the parallels
        integral_0 = tf.divide(tf.reduce_sum(
            delta_s * (tf.add(tf.concat([tf.slice(integrand_0, [1], [-1]), [1]], 0), integrand_0))), 2.,
            name='integral_0')

        # frank = integral + k24 * saddle_splay
        frank = tf.add(integral, k24 * saddle_splay, name='frank')

        # purge the energy from the energy of the parallels
        # energy = frank energy of a - frank energy of the parallels
        compared_energy = tf.subtract(frank, integral_0, name='energy')

    return compared_energy
