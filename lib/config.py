"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import numpy as np

###############################################
# analytical constraints for the approximator #
###############################################

# is the approximator analytically constrained to be 0 at 0?
a_0 = True

# is the approximator analytically constrained to be 0 at 1?
a_1 = False

#############
# samplings #
#############

# fineness of sigma sampling
s_step = 0.001
# the last value of s should be 1
s_last = 1.
# the index of s=1
s_index = -1

# sigma sampling
s_sampling = np.arange(0, s_last + s_step, s_step)

# psi sampling
psi_sampling = np.pi * np.arange(0, 2.01, 0.01)

##################################
# parameters of the approximator #
##################################

# epsilon is the half-range of initialization of the variables
epsilon = 0.3

# depth
depth = 5

# dimension of the hidden layers
size = 10

##################################
# parameters of the optimization #
##################################

# number of iterations: after performing the minimal number of iterations,
# the optimization continues until the best results is not updated for update_steps iterations
update_steps = 0

# learning rate
learning_rate = 0.0001

# optimizer_method: choose the method of optimization, between:
# 'GD' = Gradient Descent Optimizer
# 'Adam' = Adam Optimizer
# 'Adadelta' = Adadelta Optimizer
optimizer_method = 'Adam'

# seed
seed = 123

###############
# checkpoints #
###############

# should checkpoints of optimization been kept?
keep_checkpoints = False
# checkpoints folder
checkpoints_home = 'checkpoints'

###################
# plots utilities #
###################

# should the interactive plots be shown during the optimization?
plot_on = True

# frequency of printing the energy value
print_stride = 500
# frequency of plotting the iterative plot
plot_stride = print_stride

# names of parameters
k3_name = '$k_{3}$'
k24_name = '$k_{24}$'

# size of figures (in inches)
fig_size = [12.5, 6.5]

# font
font_size = 14
label_factor = 0.75

# parallels = the 0-function
par = False
color_par = 'gold'
label_par = r'$a_{\rm par}$'

# upper lin = eta * sigma
lin = True
color_lin = 'green'
label_lin = r'$a_{\rm lin}=\eta\sigma$'

# qua = eta * sigma * (1- beta * sigma)
qua = True
color_qua = 'red'
if not a_1:
    label_qua = r'$a_{\rm qua}=\eta\sigma(1-\beta\sigma)$'
else:
    label_qua = r'$a_{\rm qua}=\eta\sigma(1-\sigma)$'
max_qua = True
color_qua_max = 'darkred'
label_qua_max = r'max of $a_{\rm qua}$'

# cyl = sin(gamma)
# this is the expected solution for eta=0 and k24>1
cyl = True
color_cyl = 'red'
label_cyl = r'$a_{\rm cyl}=\sin\gamma$'
max_cyl = True
color_cyl_max = 'darkred'
label_cyl_max = r'max of $a_{\rm cyl}$'

# initial function
initial = False
color_initial = 'black'
label_initial = r'$a_{\rm initial}$'

# best and step approximators
color_step = 'rebeccapurple'
color_step_max = 'indigo'
color_alpha_step = 'mediumpurple'
color_best = 'darkblue'
label_best = r'$a_{\rm min}$'
max_best = True
color_best_max = 'midnightblue'
label_best_max = 'max of ' + label_best
color_alpha_best = 'royalblue'
label_alpha_best = r'$\alpha_{\rm min}$'
