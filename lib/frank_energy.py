"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import os
import sys

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import csv

import lib.config as c
import lib.functions as f
import lib.plots as p

from lib.varia import safe_mkdir, remove_checkpoints


def optimize_frank_energy(eta_val, k3_val, k24_val, run_name, steps, save_results=False, plot_show=True, results_home='', seed=c.seed):
    """run_frank optimizes the Frank's Energy.

        Args:
            eta_val: scalar (geometric parameter);
            k3_val: scalar (elastic constant);
            k24_val: scalar (elastic constant);
            run_name: string (experiment name);
            steps: integer (the minimal number of iterations of the optimization to be performed);
            save_results: boolean (if True, both the minimizer and the final plot are saved);
            results_home: string (results base folder);
            seed: integer (seed fof random generators).
    """

    # do not shrink printouts
    np.set_printoptions(threshold=sys.maxsize)

    # interactive plot on
    plt.ion()

    # reset tf graph
    tf.reset_default_graph()

    # variables initializer - alternative choice: init = tf.constant_initializer(c.epsilon)
    init = tf.random_uniform_initializer(-c.epsilon, c.epsilon, seed=seed)
    init_name = 'random_uniform'

    ##############
    # file names #
    ##############

    # path for storing variables after optimization
    save_name = c.checkpoints_home + '/{0}.ckpt'.format(run_name)

    # path for saving results
    resuls_name = results_home + '/{0}.csv'.format(run_name)

    # path for saving plot
    image_name = results_home + '/{0}.svg'.format(run_name)

    ################
    # placeholders #
    ################

    # placeholders for sigma sampling
    s = tf.placeholder(tf.float32, shape=None, name='s')

    # placeholders for physical parameters
    eta = tf.placeholder(tf.float32, shape=None, name='eta')
    k3 = tf.placeholder(tf.float32, shape=None, name='k3')
    k24 = tf.placeholder(tf.float32, shape=None, name='k24')

    #############
    # functions #
    #############

    # tf function
    # a_par = parallels = 0-function
    a_par = f.parallels(s=s)
    da_par = f.derivative(a=a_par, s=s)

    # numpy functions
    # a_qua = qua_function: eta * s * (1 - beta(eta, k24, a_1) * s)
    a_qua = None
    if c.qua and k24_val <= 1:
        a_qua = f.qua_function(s=c.s_sampling, eta=eta_val, k24=k24_val)

    # a_cyl = cylinder: sin(gamma(s, k3, k24))
    a_cyl = None
    if c.cyl and k24_val > 1:
        a_cyl = f.cylinder(s=c.s_sampling, k3=k3_val, k24=k24_val)

    # a_lin = lin_function: eta * s
    a_lin = None
    if c.lin:
        a_lin = f.lin_function(s=c.s_sampling, eta=eta_val)

    ################
    # approximator #
    ################

    a = f.approximator_tanh(s=s, init=init)
    da = f.derivative(a=a, s=s)

    ##########
    # energy #
    ##########

    energy = f.core_energy(s=s, a=a, da=da, eta=eta, k3=k3, k24=k24, scope='core_energy_a')

    energy_par = f.core_energy(s=s, a=a_par, da=da_par, eta=eta, k3=k3, k24=k24,
                               scope='core_energy_par')

    ##############
    # dictionary #
    ##############

    # feed dictionary
    dictionary = {s: c.s_sampling, eta: eta_val, k3: k3_val, k24: k24_val}

    #############
    # optimizer #
    #############

    # create optimizer
    with tf.name_scope('optimizer'):
        optimizer = None
    if c.optimizer_method == 'GD':
        optimizer = tf.train.GradientDescentOptimizer(c.learning_rate).minimize(energy)
    elif c.optimizer_method == 'Adam':
        optimizer = tf.train.AdamOptimizer(c.learning_rate).minimize(energy)
    elif c.optimizer_method == 'Adadelta':
        optimizer = tf.train.AdadeltaOptimizer(c.learning_rate).minimize(energy)
    else:
        print('Wrong optimizer_method')
        exit()

    # add ops to save and restore_model all the variables
    saver = tf.train.Saver()

    ####################
    # initial printout #
    ####################

    print()
    print()
    print('PARAMETERS')
    print()
    print('Experiment parameters:')
    print()
    print('eta = {0}'.format(eta_val))
    print('k3 = {0}'.format(k3_val))
    print('k24 = {0}'.format(k24_val))
    print()
    print('Process parameters (see lib/config):')
    print()
    print('a_0 = {0}'.format(c.a_0))
    print('a_1 = {0}'.format(c.a_1))
    print()
    print('s_step = {0}'.format(c.s_step))
    print()
    print('epsilon = {0}'.format(c.epsilon))
    print('depth = {0}'.format(c.depth))
    print('size = {0}'.format(c.size))
    print()
    print('steps = {0}'.format(steps))
    print('update steps = {0}'.format(c.update_steps))
    print('learning_rate = {0}'.format(c.learning_rate))
    print('optimizer_method = {0}'.format(c.optimizer_method))
    print('seed =  {0}'.format(seed))
    print('init =  {0}'.format(init_name))
    print()
    print()

    ################
    # optimization #
    ################

    # initializer for the variables
    initializer = tf.global_variables_initializer()

    # open the Session and optimize
    with tf.Session() as session:

        # initialize the variables
        session.run(initializer)

        # create a summary writer, add the 'graph' to the event file
        if not os.path.exists('graphs'):
            os.makedirs('graphs')
        writer = tf.summary.FileWriter('graphs', session.graph)

        # compute the energy of the parallels
        a_par_val = session.run(a_par, feed_dict=dictionary)
        energy_par_val = session.run(energy_par, feed_dict=dictionary)

        # compute a and its energy
        a_initial = session.run(a, feed_dict=dictionary)
        energy_initial = session.run(energy, feed_dict=dictionary)

        # initialize the best energy
        energy_best = energy_initial
        # step_best is the iteration at which the best approximator has been found
        step_best = 0
        # counter_best counts the number of updates of the best approximator
        counter_best = 0

        # training: search actual network parameters for steps (the selected minimal number) of iterations
        print()
        print()
        print('OPTIMIZING PHASE')
        print()
        temp = ' temporary '
        if c.keep_checkpoints:
            temp = ' '
        print('Checkpoints for optimization will' + temp + 'be saved in folder: {0}'.format(c.checkpoints_home))
        print('(See lib/config)')
        print()

        safe_mkdir(c.checkpoints_home)
        for i in range(steps + 1):

            # compute, print and check the energy at this step
            energy_step = session.run(energy, feed_dict=dictionary)
            if i % c.plot_stride == 0:
                print('Step {0}: Energy = {1}, (Minimum value = {2})'.format(i, energy_step, energy_best))
            if np.isnan(energy_step):
                print('Step {0}: Energy = {1}'.format(i, energy_step))
                print('Too high epsilon ({0}) or learning rate ({1})'.format(c.epsilon, c.learning_rate))
                exit()

            # update the best energy
            if energy_step < energy_best:
                energy_best = energy_step
                step_best = i
                counter_best = counter_best + 1

            # iterative plot, when plot_on
            if plot_show and c.plot_on and ((i < c.plot_stride and i % 50 == 0) or i % c.plot_stride == 0):
                a_step = session.run(a, feed_dict=dictionary)
                p.plot_all(s=c.s_sampling, a=a_step, a_par=a_par_val, a_qua=a_qua, a_cyl=a_cyl,
                           a_initial=a_initial,
                           a_lin=a_lin,
                           compared_energy=energy_step,
                           counter=i,
                           eta_val=eta_val, k3_val=k3_val, k24_val=k24_val)
                plt.show()
                plt.pause(0.0001)

            # run the optimizer
            session.run(optimizer, feed_dict=dictionary)

        # save variables
        saver.save(session, save_name)

        # training: search actual network parameters until the steps before two updates reaches c.update_steps
        # update_best is the number of past iterations without update of the approximator
        update_best = 0
        i = steps
        while update_best < c.update_steps:

            # compute, print and check the energy at this step
            energy_step = session.run(energy, feed_dict=dictionary)
            if i % c.plot_stride == 0:
                print('Step {0}: Energy = {1}, (Minimum value = {2})'.format(i, energy_step, energy_best))
            if np.isnan(energy_step):
                print('Step {0}: Energy'.format(i, energy_step))
                print('Too high epsilon ({0}) or learning rate ({1})'.format(c.epsilon, c.learning_rate))
                exit()

            # update the best result
            if energy_step < energy_best:
                saver.save(session, save_name)
                energy_best = energy_step
                step_best = i
                counter_best = counter_best + 1
                update_best = 0
            else:
                update_best = update_best + 1

            # iterative plot, when plot_on
            if plot_show and c.plot_on and i % c.plot_stride == 0:
                a_step = session.run(a, feed_dict=dictionary)
                p.plot_all(s=c.s_sampling, a=a_step, a_par=a_par_val, a_qua=a_qua, a_cyl=a_cyl,
                           a_initial=a_initial,
                           a_lin=a_lin,
                           compared_energy=energy_step,
                           counter=i,
                           eta_val=eta_val, k3_val=k3_val, k24_val=k24_val)
                plt.pause(0.0001)

            # run the optimizer
            session.run(optimizer, feed_dict=dictionary)

            # update i
            i = i + 1

        # restore_model
        saver.restore(session, save_name)

        # compute a_best and its energy
        a_best = session.run(a, feed_dict=dictionary)
        energy_best = session.run(energy, feed_dict=dictionary)
        # maximum value of a_best
        max_best = np.amax(np.abs(a_best))
        argmax_best = c.s_sampling[np.argmax(np.abs(a_best))]

        # delete checkpoints
        if not c.keep_checkpoints:
            remove_checkpoints(file_name=save_name, folder_name=c.checkpoints_home)

        # print results
        print()
        print()
        print('RESULTS')
        print()
        print('Energy of the parallels: ', energy_par_val)
        print()
        print('Minimizer a_min found at step: ', step_best)
        print('After {0} updates'.format(counter_best))
        print('With Energy: ', energy_best)
        print('Maximum value: ', max_best)
        print('At sigma: ', argmax_best)
        print()

        if k24_val > 1 and c.cyl:
            sup_diff = np.amax(np.abs(np.abs(a_cyl) - np.abs(a_best)))
            print('Sup-norm difference with a_cyl: ', sup_diff)
            print()

        print()

        # plot results
        plt.ioff()
        p.plot_all(s=c.s_sampling, a=a_best, a_par=a_par_val, a_qua=a_qua, a_cyl=a_cyl, a_initial=a_initial,
                   a_lin=a_lin,
                   compared_energy=energy_best,
                   eta_val=eta_val, k3_val=k3_val, k24_val=k24_val)

        # save results
        if save_results:
            with open(resuls_name, 'w') as results_file:
                writer_results = csv.writer(results_file)
                writer_results.writerow(a_best)
            print('Minimizer a_min has been saved in {0}'.format(resuls_name))
            plt.savefig(image_name)
            print('Plot has been saved in {0}'.format(image_name))

        print()
        print()
        print('PROCESS COMPLETE')

        writer.close()

        if plot_show:
            plt.show()

    return
