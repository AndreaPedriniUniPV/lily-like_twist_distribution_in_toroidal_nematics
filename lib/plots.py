"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D

import lib.config as c
import lib.functions as func


def plot_all(s, a, a_par=None, a_qua=None, a_cyl=None, a_lin=None, a_initial=None, compared_energy=None,
             counter=None,
             eta_val=None, k3_val=None, k24_val=None):
    """
    plot_all plots a, alpha and argmax(alpha)
    """
    # create figure
    fig = plt.figure(num=1, figsize=c.fig_size)
    fig.clear()

    # create subplots
    gs0 = gridspec.GridSpec(4, 3, width_ratios=[2.5, 1.5, 3], height_ratios=[1, 1, 1, 1])
    gs0.update(left=0.05, right=0.95, bottom=0.05, top=0.85, wspace=0.0, hspace=0.0)

    ax_functions = plt.subplot(gs0[:2, 0])

    ax_angle = plt.subplot(gs0[:, 2], projection='3d')

    ax_argmax = plt.subplot(gs0[3, 1], projection='polar', frameon=False)

    ax_step = plt.subplot(gs0[3, 0], frameon=False)

    #############
    # functions #
    #############

    # functions for comparison
    plt.sca(ax_functions)
    ax_functions.cla()
    if c.par and a_par is not None:
        ax_functions.plot(s, a_par, color=c.color_par, linestyle='-', label=c.label_par)
    if c.lin and a_lin is not None:
        ax_functions.plot(s, a_lin, color=c.color_lin, linestyle='-', label=c.label_lin)
    if c.qua and a_qua is not None:
        ax_functions.plot(s, a_qua, color=c.color_qua, linestyle='-', label=c.label_qua)
        if c.max_qua:
            max_test = np.amax(a_qua)
            argmax_test = c.s_sampling[np.argmax(a_qua)]
            ax_functions.plot((argmax_test, argmax_test), (0, max_test), color=c.color_qua_max, linestyle=':',
                              label=c.label_qua_max + '\n' + r'at $\sigma = ${0:.3f}'.format(argmax_test))
    if c.cyl and a_cyl is not None:
        ax_functions.plot(s, a_cyl, color=c.color_cyl, linestyle='-', label=c.label_cyl)
        if c.max_cyl:
            max_cyl = np.amax(a_cyl)
            argmax_cyl = c.s_sampling[np.argmax(a_cyl)]
            ax_functions.plot((argmax_cyl, argmax_cyl), (0, max_cyl), color=c.color_cyl_max, linestyle=':',
                              label=c.label_cyl_max + '\n' + r'at $\sigma = ${0:.3f}'.format(argmax_cyl))
    if c.initial and a_initial is not None:
        ax_functions.plot(s, np.abs(a_initial), color=c.color_initial, linestyle='-', label=c.label_initial)

    # current step
    color_now = c.color_best
    color_now_max = c.color_best_max
    color_now_alpha = c.color_alpha_best
    if counter is not None:
        color_now = c.color_step
        color_now_max = c.color_step_max
        color_now_alpha = c.color_alpha_step
    ax_functions.plot(s, np.abs(a), color=color_now, linestyle='-', label=c.label_best)
    if c.max_best:
        max_a = np.amax(np.abs(a))
        argmax_a = c.s_sampling[np.argmax(np.abs(a))]
        ax_functions.plot((argmax_a, argmax_a), (0, max_a), color=color_now_max, linestyle=':',
                          label=c.label_best_max + '\n' + r'at $\sigma = ${0:.3f}'.format(argmax_a))

    # axis options
    ax_functions.spines['bottom'].set_color('gray')
    ax_functions.spines['top'].set_color('gray')
    ax_functions.spines['left'].set_color('gray')
    ax_functions.spines['right'].set_color('gray')

    ax_functions.grid(color='silver', linewidth=1)
    ax_functions.set_xbound(lower=-0.001, upper=1.001)
    ax_functions.set_xticks(np.arange(0.0, 1.1, 0.1))
    ax_functions.set_ybound(lower=-0.001)
    ax_functions.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
    ax_functions.yaxis.major.formatter._useMathText = True
    ax_functions.get_yaxis().get_offset_text().set_x(-0.15)

    #########
    # angle #
    #########
    plt.sca(ax_angle)
    ax_angle.cla()

    # angle alpha
    alpha_best = func.alpha(np.abs(a), c.s_sampling, c.psi_sampling, eta_val)
    psi = c.psi_sampling

    # maximum values of alpha for each phi
    max_alpha_best_s = np.amax(np.abs(alpha_best), axis=1)
    argmax_alpha_best_ind = np.argmax(np.abs(alpha_best), axis=1)
    argmax_alpha_best_s = []
    for ind in argmax_alpha_best_ind:
        argmax_alpha_best_s.append(c.s_sampling[ind])

    # cartesian coordinates
    f, g = np.meshgrid(s, psi)
    x, y = f * np.cos(g), f * np.sin(g)

    ax_angle.plot_surface(x, y, alpha_best, color=color_now_alpha, alpha=0.8)
    ax_angle.plot([], [], [], color=color_now_alpha, label=c.label_alpha_best, alpha=0.8)
    x_best, y_best = argmax_alpha_best_s * np.cos(psi), argmax_alpha_best_s * np.sin(psi)
    ax_angle.plot(x_best, y_best, max_alpha_best_s, color=color_now_max, label='max of ' + c.label_alpha_best)

    # axis options
    ax_angle.grid(color='silver', linewidth=1)

    #####################
    # argmax (of angle) #
    #####################
    plt.sca(ax_argmax)
    ax_argmax.cla()
    ax_argmax.plot(psi, argmax_alpha_best_s, color=color_now_max)

    # axis options
    ax_argmax.grid(color='silver', linewidth=1)
    ax_argmax.set_xticks(np.pi * np.arange(0, 2, 1 / 4))
    ax_argmax.set_xticklabels([r'$0$', r'$\dfrac{\pi}{4}$', r'$\dfrac{\pi}{2}$', r'$\dfrac{3\pi}{4}$', r'$\pi$',
                               r'$\dfrac{5\pi}{4}$', r'$\dfrac{3\pi}{2}$', r'$\dfrac{7\pi}{4}$'])
    ax_argmax.set_ylim(0, 1.01)

    ########
    # step #
    ########
    plt.sca(ax_step)
    ax_step.cla()
    if compared_energy is not None:
        ax_step.text(0.0, 0.4, 'Energy = {0}'.format(compared_energy), fontsize=c.font_size)
    if counter is not None:
        ax_step.text(0.0, 0.9, 'Step {}'.format(counter), fontsize=c.font_size)

    # axis options
    ax_step.grid(color='silver', linewidth=1)
    ax_step.set_xticks([])
    ax_step.set_yticks([])

    ##########
    # legend #
    ##########
    ax_functions.legend(bbox_to_anchor=[1.03, 1.], loc=2, fontsize=c.label_factor * c.font_size, frameon=False)
    ax_angle.legend(loc=1, fontsize=c.label_factor * c.font_size, frameon=False)

    ##########
    # titles #
    ##########
    suptitle = '$\eta$ = {0:.3f},   {1} = {2:.3f},   {3} = {4:.3f}'.format(eta_val, c.k3_name,
                                                                           k3_val, c.k24_name,
                                                                           k24_val)
    if counter is not None:
        suptitle = 'OPTIMIZING PHASE:     ' + suptitle

    plt.suptitle(suptitle, size=1.2 * c.font_size)
    ax_functions.set_title(r'Functions $a(\sigma)$', size=c.font_size)
    ax_angle.set_title(r'Angle $\alpha(\sigma,\psi)$' + '\n' + '\n', fontsize=c.font_size)
    ax_argmax.set_title(r'$\sigma_{\rm{rmt}}(\psi)$' + '\n' + '\n', fontsize=c.font_size)

    return
