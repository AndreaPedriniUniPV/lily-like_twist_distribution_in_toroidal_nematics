"""
This code is companion to the article "Non-monotonic, lily-like twist distribution in toroidal nematics", by
    Andrea Pedrini (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Marco Piastra (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;
    Epifanio G. Virga (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.

"""

import sys
import os
import datetime
import glob
import shutil


def safe_mkdir(path):
    """ Create a directory if there isn't one already. """
    try:
        os.mkdir(path)
    except OSError:
        pass


def generate_timestamp():
    return datetime.datetime.now().strftime("%Y%m%d_%H%M%S")


def experiment_name(eta, k3, k24):
    basename = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    name = '{0}_{1:.3f}_{2:.3f}_{3:.3f}_{4}'.format(basename, eta, k3, k24, generate_timestamp())
    return name


def remove_checkpoints(file_name, folder_name):
    """ Remove checkpoints file and folder. """
    print()
    for name in glob.glob(file_name + '*'):
        os.remove(name)
        print('File {0} deleted'.format(name))
    if len(os.listdir(folder_name)) == 1:
        shutil.rmtree(folder_name)
        print('Folder {0} deleted'.format(folder_name))


class stream_tee(object):
    # Based on https://gist.github.com/327585 by Anand Kunal
    def __init__(self, stream1, stream2):
        self.stream1 = stream1
        self.stream2 = stream2
        self.__missing_method_name = None  # Hack!

    def __getattribute__(self, name):
        return object.__getattribute__(self, name)

    def __getattr__(self, name):
        self.__missing_method_name = name  # Could also be a property
        return getattr(self, '__methodmissing__')

    def __methodmissing__(self, *args, **kwargs):
        # Emit method call to the log copy
        callable2 = getattr(self.stream2, self.__missing_method_name)
        callable2(*args, **kwargs)

        # Emit method call to stdout (stream 1)
        callable1 = getattr(self.stream1, self.__missing_method_name)
        return callable1(*args, **kwargs)


if __name__ == '__main__':
    logfile = open("blah.txt", "w+")

    sys.stdout = stream_tee(sys.stdout, logfile)

    print(generate_timestamp())
    print("# Now, every operation on sys.stdout is also mirrored on logfile")

    logfile.close()
