# Non-monotonic, lily-like twist distribution in toroidal nematics - accompanying ESI

This code is companion to the article **_"Non-monotonic, lily-like twist distribution in toroidal nematics"_**, submitted to *Soft Matter*.

#### Authors
*Andrea Pedrini* (andrea.pedrini@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;

*Marco Piastra* (marco.piastra@unipv.it) - Dipartimento di Ingegneria Industriale e dell'Informazione, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy;

*Epifanio G. Virga* (eg.virga@unipv.it) - Dipartimento di Matematica, Università di Pavia, via Ferrata 5, 27100 Pavia, Italy.


## Purpose

The main purpose of this code is to find a numeric approximation to the global minimalizer 
of Frank's elastic energy free functional, in the complex setting of toroidal nematics.

For any further detail about the mathematical methods and for a discussion of the results, we refer the reader to the associated article. 

## Prerequisites

Use python 3.x

See also requirements.txt

## Files
The following files are included in the online repository:

* README.md   -- this readme
* run.py      -- the main file
* lib         -- folder containing:
    * config.py     -- parameters for advanced configuration
    * functions.py  -- source code for functions
    * plot.py       -- source code for plots
    * varia.py      -- source code for utilities
* requirements.txt

## Test
To test that everything is properly working, execute:
```
python3 run.py
```
A default experiment will start and a plot window will open.

The plot window contains three plots, which are updated first every 50 steps and then every 500 steps -
the step counter is displayed on the left side of the window, below the first plot.

The run is complete when the step counter disappears. 

Three new folders are created during the run:
* results   -- contains the results of the experiment:
    * a .svg file for the final plot
    * a .csv file for the minimizer (1001 values)
* logs      -- contains the log file of the experiment
* graphs    -- contains the graph of the experiment that can be read by TensorBoard (see TensorFlow documentation)
    
## Experiments
File run.py can be run with different parameters:

* eta       -- geometric parameter: ratio between the radii of the torus (default=0.5)
* k3        -- elastic constant: ratio of the bend to twist elastic constants (default=8.5)
* k24       -- elastic constant: ratio of the saddle-splay to twist elastic constants (default=0.1)
* steps     -- minimal number of iterations performed (default=6000)
* plot      -- if True: the experiment results are displayed in a plot window (default=True)
* save      -- if True: the experiment results and plots are saved (as .csv and .svg files, in the results folder) (default=True)

Example: the code
```
python3 run.py --eta 0.5 --k3 7 --k24 0.8 --steps 7000 --plot True --save True
```
starts an experiment where eta=0.5, k3=7, k24=0.8, 
the minimal number of iterations performed is 7000, the plot is displayed and both the minimizer and 
the final plot are saved.

#### Plot window
We give here a brief description of what is shown in the plot window.

The title contains the values of the geometric and elastic constants eta, k3 and k24
for the experiment.
While the optimization is running, the title starts with the tag 'OPTIMIZATION PHASE',
the tag disappears when the optimization is complete.

The first plot (top left) displays the minimizer a_min and the test functions a_lin and a_qua (for k24<=0) or a_cyl (for k24>0).
* a_min changes color from purple to blue when the optimization is complete
* a_lin is the linear test function used in Koning et al. (ref.6 in the article)
* a_qua is the rational test function used in Pedrini and Virga (ref.14 in the article)
* a_cyl is the exact solution found in a cylinder by Davidson et al. (ref.17 in the article)

The second plot (right) displays the angle alpha_min computed according to a_min (see equation 12 in the article).

The third plot (bottom-middle) displays the rim (see the article for more details).

On the bottom-left corner the step counter and the value of the energy are displayed.
The step counter disappears when the computation is complete.
The energy of the reference configuration of the parallels of the torus is 0.

## Examples

The following examples relate to more specific cases described in the article.

#### Example 1
```
python3 run.py --eta 0.5 --k3 8.5 --k24 0.1 --steps 6000
```

Here eta=0.5, k3=8.5, k24=0.1.
This is the example presented in Figg. 5 and 6 of the article.

The result of this experiment is a non-zero function: for this particular choice of the parameters
the parallels of the torus are not stable. In fact, both the
configurations with the displayed alpha_min and its dual -alpha_min have less energy.

#### Example 2
```
python3 run.py --eta 0.5 --k3 2 --k24 0.5 --steps 6000
```

Here eta=0.5, k3=2, k24=0.5.

The result of this experiment is the zero function - up to a negligible error due to numerical appoximations: for this particular choice of the parameters,
the parallels of the torus are stable.

The energy given by the minimizer is essentially 0 and the second plot (alpha_min)
is the null function. The third plot here is not relevant.

#### Example 3
```
python3 run.py --eta 0.01 --k3 1 --k24 2 --steps 10000
```

Here eta=0.01, k3=1, k24=2.
This is the example presented in Fig. 9 of the article.

The result of this experiment is a validation of the method.
When eta is small, the torus can be approximated with a cylinder and we can compare a_min
with the analytical solution a_cyl found by Davidson et al. (ref.16, see equation 18 in the article):
as shown by the first plot, they are practically the same function.

#### Example 4
```
python3 run.py --eta 0.01 --k3 8 --k24 4 --steps 12000
```

Here eta=0.01, k3=8, k24=4.
This is the example presented in Fig. 12 of the article.

The result of this experiment provides a further validation to the method, again in the cylinder limit.
Note that, for this particular choice of the elastic constants, a_cyl is more elaborate (with also a change in its concavity)
that the one in Example 3.

## Advanced configuration 
File config.py collects a number of parameters that can be used for advanced configuration of the program.

Here a brief description of the most relevant ones.

#### a_0
By default, the minimizer a_min is forced to be 0 at 0 (see the article for more details).
This constrain is obtained by setting `a_0 = True`.

Setting `a_0 = False` makes the minimizer free at 0.

#### s_step
`s_step` is the sampling step for the values of sigma in the interval [0, 1]. Increasing this value makes the computation faster but less precise.

#### depth and size, epsilon
`depth` is the number of layers of the approximator (i.e. the deep neural network).

`size` is the dimension of vectors and square matrices in which the scalar parameters of the approximator are arranged 
(i.e. the trainable parameters of the approximators - see equation 20 in the article).

`epsilon` determines the range for the random initialization of vectors and square matrices 
(i.e. at the beginning of the training process).


#### update_step
When `update_step` is not 0, the optimization performs the minimal number of iteration given in `steps`,
and then continues until the minimizer a_min is not updated for `update_step` iterations (early stopping).

#### learning_rate
`learning_rate` is the speed of the optimization process. In general, an higher value 
produces better results in the first steps (i.e. it approaches the minimal energy more quickly) 
but reaching the optimal shape requires more steps afterwards.  

#### optimizer_method
`optimizer_method` is the method used to perform the gradient descent (GD) optimization.
In this code three different methods are implemented: plain GD, Adam and Adadelta 
(default: Adam).

#### checkpoints
If `keep_checkpoints` is True, checkpoints are saved and kept in the corresponding folder.
This means that all parameters for the approximator are saved, so that they can be reused in further computations.

#### plot utilities
All parameters in this section relate to visualization details. In particular, it is possible to speed up the whole optimization process
by turning off the interactive plots or choosing which functions will be actually plotted. 